import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import {MessageService} from './service/message.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  //  TODO 4 ::: add service to the providers in the module
  providers: [
      MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
