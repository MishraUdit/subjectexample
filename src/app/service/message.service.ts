// TODO 3 ::: created a service with the command: ng g s service/message

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class MessageService {
    // TODO 5 ::: created a public instance of the Subject of type String
    public message = new Subject<string>();

    // TODO 6 ::: method to publish this value to all the subscribers that have already subscribed to the message
    setMessage(value: string) {
        this.message.next(value);
    }
}
