import {Component, OnDestroy, OnInit} from '@angular/core';
import { MessageService } from './service/message.service';
import { Subscription } from 'rxjs';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements  OnInit, OnDestroy{

    // TODO 10 ::: local var
    message: string;
    subscription: Subscription;
    // TODO 11 ::: injection of service
    constructor(public messageService: MessageService) { }

    // TODO 12 ::: inside onInit subscribe the subject to get updated value
    ngOnInit() {
        this.subscription = this.messageService.message.subscribe(
            (message) => {
                // TODO 13 ::: setting updated value to the class variable
                this.message = message;
            }
        );
    }

    // TODO 14 ::: unsubscribe the subscription inside onDestroy
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}