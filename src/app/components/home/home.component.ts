import { Component } from '@angular/core';
import { MessageService } from '../../service/message.service';
@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})

export class HomeComponent {
    // TODO 7 ::: injection of the service class
    constructor(public messageService:MessageService) { }

    // TODO 9 ::: setting argument of the method to the subject by calling setter
    setMessage(event) {
        console.log(event.value);
        this.messageService.setMessage(event.value);
    }
}